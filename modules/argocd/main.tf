
module "argocd" {
  source  = "terraform-module/release/helm"
  version = "2.8.0"

  namespace  = "argocd"
  repository = "https://argoproj.github.io/argo-helm"

  app = {
    name             = "argocd"
    version          = "5.16.2"
    chart            = "argo-cd"
    wait             = false
    recreate_pods    = false
    deploy           = 1
    create_namespace = true
  }
  values = var.values

  set = var.set

  set_sensitive = var.set_sensitive
}

resource "kubernetes_manifest" "main-app" {
  depends_on = [
    module.argocd
  ]

  manifest = {
    apiVersion = "argoproj.io/v1alpha1"
    kind       = "Application"
    metadata = {
      name      = "main-app"
      namespace = "argocd"
      finalizers = [
        "resources-finalizer.argocd.argoproj.io"
      ]
    }
    spec = {
      project = "default"
      source = {
        repoURL        = var.repo_url
        targetRevision = var.targetRevision
        path           = var.argocd_applications_path
      }
      destination = {
        server    = "https://kubernetes.default.svc"
        namespace = "argocd"
      }
      syncPolicy = {
        automated = {}
      }
    }
  }
}
