variable "values" {
  description = "Extra values"
  type        = list(string)
  default     = []
}

variable "set" {
  description = "Value block with custom STRING values to be merged with the values yaml."
  type = list(object({
    name  = string
    value = string
  }))
  default = null
}

variable "set_sensitive" {
  description = "Value block with custom sensitive values to be merged with the values yaml that won't be exposed in the plan's diff."
  type = list(object({
    path  = string
    value = string
  }))
  default = null
}
variable "cluster_name" {
  type        = string
  description = "cluster name"
}

variable "targetRevision" {
  type        = string
  description = "Target revision on repository"
  default     = "main"
}

variable "argocd_applications_path" {
  type        = string
  description = "path to argocd applicationSets"
}

variable "repo_url" {
  type        = string
  description = "repository url"
}
