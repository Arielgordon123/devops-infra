terraform {
  source = "git::https://gitlab.com/Arielgordon123/devops-infra.git//modules/argocd?ref=v0.0.4"
}

include {
  path = find_in_parent_folders()
}

locals {
  environment = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region      = read_terragrunt_config(find_in_parent_folders("region.hcl"))

}

dependency "eks" {
  config_path = "../eks"

}

dependencies {
  paths = ["../eks"]
}
inputs = {
  cluster_name = dependency.eks.outputs.cluster_name

  argocd_applications_path = "k8s/apps/"
  repo_url                 = "https://gitlab.com/Arielgordon123/devops-infra.git"
  values                   = [file("${get_terragrunt_dir()}/${path_relative_from_include()}/../k8s/argocd/values.yml")]
}
