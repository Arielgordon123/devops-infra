
terraform {
  source = "tfr://registry.terraform.io/terraform-aws-modules/route53/aws//modules/zones?version=2.10.1"
}

prevent_destroy = true

include {
  path = find_in_parent_folders()
}

locals {
  environment = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region      = read_terragrunt_config(find_in_parent_folders("region.hcl"))

}



inputs = {
  create = true

  zones = {
    "the-counter.cloud" = {
      comment = "the-counter.cloud"
    }
  }

  tags = {
    app         = local.environment.locals.app_prefix
    environment = local.environment.locals.environment
  }
}
