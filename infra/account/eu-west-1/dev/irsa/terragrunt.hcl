terraform {
  source = "tfr://registry.terraform.io/terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks?version=5.9.2"
}

include {
  path = find_in_parent_folders()
}

locals {
  environment = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region      = read_terragrunt_config(find_in_parent_folders("region.hcl"))

}

dependency "eks" {
  config_path = "../eks"
}

dependency "route53" {
  config_path = "../route53"
}

dependencies {
  paths = ["../eks", "../route53"]
}

inputs = {
  role_name_prefix                       = "irsa-module"
  create_role                            = true
  attach_load_balancer_controller_policy = true
  attach_external_dns_policy             = true
  external_dns_hosted_zone_arns          = [values(dependency.route53.outputs.route53_zone_zone_arn)[0]]

  oidc_providers = {
    main = {
      provider_arn               = dependency.eks.outputs.oidc_provider_arn
      namespace_service_accounts = ["kube-system:aws-load-balancer-controller", "external-dns:external-dns"]
    }
  }
}
