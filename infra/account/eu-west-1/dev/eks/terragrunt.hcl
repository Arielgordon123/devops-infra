terraform {
  source = "tfr://registry.terraform.io/terraform-aws-modules/eks/aws//.?version=19.0.4"
}

include {
  path = find_in_parent_folders()
}

locals {
  environment = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region      = read_terragrunt_config(find_in_parent_folders("region.hcl"))

}

dependency "vpc" {
  config_path = "../vpc"
  mock_outputs = {
    vpc_id          = "vpc-8778373"
    private_subnets = ["10.11.12.13/14", "12.13.14.15/16"]
  }
}

dependencies {
  paths = ["../vpc"]
}

inputs = {
  cluster_name                   = "${local.environment.locals.app_prefix}-cluster"
  cluster_version                = "1.24"
  cluster_endpoint_public_access = true

  cluster_addons = {
    coredns = {
      most_recent = true
    }
    kube-proxy = {
      most_recent = true
    }
    vpc-cni = {
      most_recent = true
    }
  }

  vpc_id     = dependency.vpc.outputs.vpc_id
  subnet_ids = dependency.vpc.outputs.private_subnets

  eks_managed_node_group_defaults = {
    instance_types = ["t3a.large", "t3a.xlarge"]
    capacity_type  = "SPOT"
    iam_role_additional_policies = {
      "AmazonEKSWorkerNodePolicy"          = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
      "AmazonEC2ContainerRegistryReadOnly" = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
    }
  }



  eks_managed_node_groups = {
    managed_node = {
      min_size     = 2
      max_size     = 3
      desired_size = 2
    }
  }
  tags = {
    environment = local.environment.locals.environment
  }
}
