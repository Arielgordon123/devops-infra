
terraform {
  source = "tfr://registry.terraform.io/terraform-aws-modules/acm/aws//.?version=4.3.1"
}

prevent_destroy = true

include {
  path = find_in_parent_folders()
}

locals {
  environment = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region      = read_terragrunt_config(find_in_parent_folders("region.hcl"))

}

dependency "route53" {
  config_path = "../route53"
}

dependencies {
  paths = ["../route53"]
}


inputs = {


  domain_name = "the-counter.cloud"
  zone_id     = values(dependency.route53.outputs.route53_zone_zone_id)[0]

  subject_alternative_names = [
    "*.the-counter.cloud"
  ]

  wait_for_validation = true

  tags = {
    Name = "the-counter.cloud"
  }
}
