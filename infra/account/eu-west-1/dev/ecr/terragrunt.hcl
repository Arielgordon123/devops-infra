
terraform {
  source = "tfr://registry.terraform.io/terraform-module/ecr/aws//.?version=1.0.5"
}

prevent_destroy = true

include {
  path = find_in_parent_folders()
}

locals {
  environment = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region      = read_terragrunt_config(find_in_parent_folders("region.hcl"))

}



inputs = {
  ecrs = {
    "counter-service" = {
      tags = { Service = "counter-service" }
      lifecycle_policy = {
        rules = [{
          rulePriority = 1
          description  = "keep last 50 images"
          action = {
            type = "expire"
          }
          selection = {
            tagStatus   = "any"
            countType   = "imageCountMoreThan"
            countNumber = 50
          }
        }]
      }
    }
  }
}
