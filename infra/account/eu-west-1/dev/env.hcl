locals {
    environment = "dev"
    app_name = "counter"
    vpc_cidr = "172.20.0.0/16"
    app_prefix = "${local.app_name}-${local.environment}"
}