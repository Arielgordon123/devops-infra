terraform {
  source = "tfr://registry.terraform.io/terraform-aws-modules/vpc/aws//.?version=3.18.1"
}

include {
  path = find_in_parent_folders()
}

locals {
  environment = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  region      = read_terragrunt_config(find_in_parent_folders("region.hcl"))

}

inputs = {
  aws_region     = local.region.locals.aws_region
  azs            = local.region.locals.azs
  cidr           = local.environment.locals.vpc_cidr
  vpc_name       = "${local.environment.locals.app_prefix}-vpc"
  public_subnets = [for i in range(3) : cidrsubnet(local.environment.locals.vpc_cidr, 6, i)]
  public_subnet_tags = {
    "kubernetes.io/cluster/counter-dev-cluster" = "owned"
    "type"                                      = "public-subnet"
    "kubernetes.io/role/elb"                    = "1"
  }
  private_subnets = [for i in range(3, 6) : cidrsubnet(local.environment.locals.vpc_cidr, 6, i)]
  private_subnet_tags = {
    "kubernetes.io/cluster/counter-dev-cluster" = "owned"
    "type"                                      = "private-subnet"
    "kubernetes.io/role/internal-elb"           = "1"
  }
  enable_nat_gateway     = true
  single_nat_gateway     = true
  one_nat_gateway_per_az = false

  tags = {
    environment = local.environment.locals.environment
  }
}
