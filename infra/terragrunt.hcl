locals {
  region = read_terragrunt_config(find_in_parent_folders("region.hcl"))
}
terraform {
  extra_arguments "out_plan" {
    commands = [
      "plan",
    ]

    arguments = [
      "-out=${get_terragrunt_dir()}/tfplan.binary",
    ]
  }
  extra_arguments "in_apply" {
    commands = [
      "apply",
    ]

    arguments = [
      "${get_terragrunt_dir()}/tfplan.binary",
    ]
  }
  after_hook "after_hook_plan" {
    commands = ["plan"]
    execute  = ["sh", "-c", "cd ${get_parent_terragrunt_dir()}/${path_relative_to_include()}; terragrunt show -json tfplan.binary > ${get_parent_terragrunt_dir()}/${path_relative_to_include()}/tfplan.json"]
  }
}
remote_state {
  backend = "s3"
  generate = {
    path      = "_backend.tf"
    if_exists = "overwrite"
  }

  config = {
    encrypt        = true
    region         = "eu-west-1"
    key            = format("%s/terraform.tfstate", path_relative_to_include())
    bucket         = format("tg-remote-state-%s", get_aws_account_id())
    dynamodb_table = format("tg-remote-locks-%s", get_aws_account_id())
  }
}

generate "main_providers" {
  path      = "main_providers.tf"
  if_exists = "overwrite"
  contents  = <<EOF

provider "kubernetes" {
  config_path = "~/.kube/config"
  exec {
    api_version = "client.authentication.k8s.io/v1"
    args        = ["eks", "get-token", "--cluster-name", var.cluster_name]
    command     = "aws"
  }
}
provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
    exec {
    api_version = "client.authentication.k8s.io/v1"
    args        = ["eks", "get-token", "--cluster-name", var.cluster_name]
    command     = "aws"
  }
  }
}
provider "aws" {

  region = "${local.region.locals.aws_region}"
}
EOF
}
